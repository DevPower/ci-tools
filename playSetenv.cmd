@REM  Environnement de demo - positionnement des variables
@ECHO OFF

SET TOOLS_DIR=D:\Developpement\Tools

SET JAVA_JRE_HOME=%TOOLS_DIR%\com\oracle\java\jre1.8.0_40
SET JAVA_JDK_HOME=%TOOLS_DIR%\com\oracle\java\jdk1.8.0_40
SET PLAY_ACTIVATOR_HOME=%TOOLS_DIR%\activator-1.3.4-minimal
SET SBT_HOME=%TOOLS_DIR%\sbt

echo JAVA_JRE_HOME : %JAVA_JRE_HOME%
echo PLAY_ACTIVATOR_HOME : %PLAY_ACTIVATOR_HOME%
echo SBT_HOME : %SBT_HOME%
echo JAVA_JDK_HOME : %JAVA_JDK_HOME%

REM   Constitution du PATH pour l'utilisation de la ligne de commande.
REM   On met nos outils au debut pour avoir la priorite sur tout ce qui
REM   pourrait deja etre configure sur la machine
PATH %JAVA_JRE_HOME%\bin;%JAVA_JDK_HOME%\bin;%SBT_HOME%\bin;%PLAY_ACTIVATOR_HOME%;%PATH%

REM   Effacement du CLASSPATH au cas ou il y en aurait un defini en local sur
REM   le poste.
SET CLASSPATH=
